package br.com.deyvison.caed.escola.model;

import br.com.deyvison.caed.escola.utils.CalculaIdade;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "aluno")
public class AlunoModel implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "O campo 'nome' não pode estar vazio.")
    @Size(max = 100)
    private String nome;

    @Column(unique = true)
    @CPF
    @NotBlank(message = "O campo 'CPF' não pode estar vazio.")
    private String cpf;

    @Column(name="data_nascimento")
    @NotNull(message = "O campo data de nascimento não pode estar vazio!")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataNascimento;

    @NotBlank(message = "O campo 'sexo' não pode estar vazio.")
    @Size(min = 1, max = 1, message = "O campo sexo deve conter apenas uma letra.")
    private String sexo;

    @Column(unique = true)
    @Email(message = "Email inválido")
    private String email;

    @Size(min = 3, max = 3)
    // Verifica a partir da Classe CalculaIdade em utils
    private String maior_idade;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone="GMT-3")
    private Date data_criacao;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone="GMT-3")
    private Date data_alteracao;

    // Getters e Setters
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }
    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSexo() {
        return sexo;
    }
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getMaior_idade() {
        int idade = new CalculaIdade().getIdade(getDataNascimento());
        if (idade >= 18) {
            return "Sim";
        } else {
            return "Não";
        }
    }

    public void setMaior_idade(String maior_idade) {
        this.maior_idade = maior_idade;
    }

    public Date getData_criacao() {
        return data_criacao;
    }
    public void setData_criacao(Date data_criacao) {
        this.data_criacao = data_criacao;
    }

    public Date getData_alteracao() {
        return data_alteracao;
    }
    public void setData_alteracao(Date data_alteracao) {
        this.data_alteracao = data_alteracao;
    }

}
