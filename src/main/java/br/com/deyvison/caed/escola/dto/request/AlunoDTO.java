package br.com.deyvison.caed.escola.dto.request;

import br.com.deyvison.caed.escola.model.AlunoModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AlunoDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "O campo 'nome' não pode estar vazio.")
    @Size(max = 100)
    private String nome;

    @Column(unique = true)
    @NotBlank(message = "O campo 'CPF' não pode estar vazio.")
    @Size(min = 11, max = 11, message = "CPF inválido.")
    private String cpf;

    @Column(name="data_nascimento")
    @NotNull(message = "O campo data de nascimento não pode estar vazio!")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataNascimento;

    @NotBlank(message = "O campo 'sexo' não pode estar vazio.")
    @Size(min = 1, max = 1, message = "O campo sexo deve conter apenas uma letra.")
    private String sexo;

    @Email(message = "Email inválido")
    private String email;

    @Size(min = 3, max = 3)
    // Verifica a partir da Classe CalculaIdade em utils
    private String maior_idade;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone="GMT-3")
    private Date data_criacao;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone="GMT-3")
    private Date data_alteracao;

//    public AlunoModel transformaParaObjeto(){
//        return new AlunoModel(id, nome, cpf, dataNascimento, sexo, email, maior_idade, data_criacao, data_alteracao);
//    }

}
