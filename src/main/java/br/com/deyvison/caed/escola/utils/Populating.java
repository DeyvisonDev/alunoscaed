package br.com.deyvison.caed.escola.utils;

import br.com.deyvison.caed.escola.model.AlunoModel;
import br.com.deyvison.caed.escola.repository.AlunoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class Populating {

    @Autowired
    AlunoRepository alunoRepository;

    //@PostConstruct
    public void saveAluno() {
        List<AlunoModel> alunoList = new ArrayList<>();
        AlunoModel aluno1 = new AlunoModel();
        aluno1.setId(1L);
        aluno1.setNome("Deyvison");
        aluno1.setCpf("11903859700");
        aluno1.setDataNascimento(LocalDate.now());
        aluno1.setSexo("M");
        aluno1.setEmail("deyvison@email.com");

        AlunoModel aluno2 = new AlunoModel();
        aluno2.setId(2L);
        aluno2.setNome("Cibele");
        aluno2.setCpf("11903859725");
        aluno2.setDataNascimento(LocalDate.now());
        aluno2.setSexo("F");
        aluno2.setEmail("cibele@email.com");

        alunoList.add(aluno1);
        alunoList.add(aluno2);

        for (AlunoModel aluno: alunoList) {
            AlunoModel saveAlunoModel = alunoRepository.save(aluno);
            System.out.println(saveAlunoModel.getId());
        }

    }

}
