package br.com.deyvison.caed.escola.controller;

import br.com.deyvison.caed.escola.exception.AlunoNotFoundException;
import br.com.deyvison.caed.escola.model.AlunoModel;
import br.com.deyvison.caed.escola.service.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("escola/alunos")
public class AlunoController {

    private AlunoService alunoService;

    @Autowired
    public AlunoController(AlunoService alunoService) {
        this.alunoService = alunoService;
    }

    // Exibe Lista de Alunos
    @GetMapping
    public ResponseEntity<List<AlunoModel>> getAlunos() {
        return ResponseEntity.ok().body(alunoService.findAll());
    }

    // Exibe por consulta de id
    @GetMapping("/{id}")
    public ResponseEntity<AlunoModel> getAlunoId(@PathVariable Long id) {
        return ResponseEntity.ok().body(alunoService.findById(id));
    }

    //Create
    @PostMapping(path = "/salvar")
    public ResponseEntity<?> saveAluno(@Valid @RequestBody AlunoModel aluno, BindingResult br) {
        if (br.hasErrors()) {
            return new ResponseEntity<>("Erro ao gravar o novo aluno, confira os dados informados", HttpStatus.BAD_REQUEST);
        }
        aluno.setData_criacao(new Date());
        return new ResponseEntity<>(alunoService.save(aluno).getNome() + " - Cadastrado com sucesso", HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> update(@PathVariable Long id,@RequestBody @Valid AlunoModel aluno) throws AlunoNotFoundException {
        aluno.setData_alteracao(new Date());
        return new ResponseEntity<>(alunoService.save(aluno).getNome() + " - Atualizado com sucesso", HttpStatus.OK);
    }

}
