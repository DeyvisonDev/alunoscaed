package br.com.deyvison.caed.escola.controller;

import br.com.deyvison.caed.escola.dto.response.MessageResponseDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatusController {

    @GetMapping(path = "/api/status")
    public String check() {
        return "Aplicação on-line!";
    }

}
