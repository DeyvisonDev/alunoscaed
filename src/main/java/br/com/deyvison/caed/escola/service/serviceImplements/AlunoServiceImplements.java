package br.com.deyvison.caed.escola.service.serviceImplements;

import br.com.deyvison.caed.escola.exception.AlunoNotFoundException;
import br.com.deyvison.caed.escola.model.AlunoModel;
import br.com.deyvison.caed.escola.repository.AlunoRepository;
import br.com.deyvison.caed.escola.service.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlunoServiceImplements implements AlunoService {

    @Autowired
    AlunoRepository alunoRepository;

    @Override
    public List<AlunoModel> findAll() {
        return (List<AlunoModel>) alunoRepository.findAll();
    }

    @Override
    public AlunoModel findById(long id) {
        return alunoRepository.findById(id).get();
    }

    @Override
    public AlunoModel save(AlunoModel aluno) {
        return alunoRepository.save(aluno);
    }

    @Override
    public AlunoModel update(Long id, AlunoModel aluno) throws AlunoNotFoundException {
        alunoRepository.findById(id).orElseThrow(() -> new AlunoNotFoundException(id));
        return alunoRepository.save(aluno);
    }

}
