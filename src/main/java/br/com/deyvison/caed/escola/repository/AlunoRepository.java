package br.com.deyvison.caed.escola.repository;

import br.com.deyvison.caed.escola.model.AlunoModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlunoRepository extends JpaRepository<AlunoModel, Long> {
}
