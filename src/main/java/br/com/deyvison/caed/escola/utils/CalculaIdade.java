package br.com.deyvison.caed.escola.utils;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CalculaIdade {

    public Integer getIdade(LocalDate data_nascimento) {
        LocalDate dataAtual = LocalDate.now();
        int anoAtual = dataAtual.getYear();
        int anoNascimento = data_nascimento.getYear();
        int idade = (anoAtual - anoNascimento);
        return idade;
    }

}
