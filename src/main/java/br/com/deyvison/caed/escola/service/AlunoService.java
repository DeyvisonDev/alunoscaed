package br.com.deyvison.caed.escola.service;

import br.com.deyvison.caed.escola.exception.AlunoNotFoundException;
import br.com.deyvison.caed.escola.model.AlunoModel;

import java.util.List;

public interface AlunoService {

    List<AlunoModel> findAll();
    AlunoModel findById(long id);
    AlunoModel save(AlunoModel aluno);
    AlunoModel update(Long id, AlunoModel aluno) throws AlunoNotFoundException;

}
