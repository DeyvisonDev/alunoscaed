## PROJETO ALUNO API- PROC. SELETIVO 018/2020 - FUNDAÇÃO CAEd

### Tecnologias utlizadas

1. Java open jdk 15;
2. IntelliJ Idea;
3. Spring Boot:
    - *Spring Boot DevTools Developer Tool*s
    - *Lombok Developer Tools*
    - *Spring Web Web*
    - *Spring Data JPA SQL*
    - *Spring Boot Actuator Ops*
    - *H2 Database SQL e MySQL* - Obs. Caso deseje usar o banco MySQL, entrar com os dados
4. Postman/Insomnia

---

### Instruções para execução

1. Abrir o projeto com o IntelliJ, e adicionar a versão jdk;
2. Edite Run/Debug e selecione a classe principal caed.deyvison.alunoapi.AlunoapiApplication;
3. Execute a aplicação, a API será executada na porta 8080.
    - Acessar através: _http://localhost:8080/escola/alunos_
    - Para acessar o banco em memória: _http://localhost:8080/h2_
    - Salvar com postman: _localhost:8080/escola/alunos/salvar_
    
_iniciei um novo projeto, adaptando para Testes, mas não consegui concluir em tempo hábil_ [GitTestes](https://github.com/deyvisonjp/alunoapi_caed)   
_Desde já agradeço a oportunidade, aprendi muito com esse processo!_    

